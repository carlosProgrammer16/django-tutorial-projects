from django.urls import path
from .views import (
    PostListView,
    PostDetailView, 
    PostCreateView,
    PostUpdateView,
    PostDeleteView,
    UserPostListView,
    search,
)
from . import views

urlpatterns = [
    path('', PostListView.as_view(), name='blog-home'),
    path('search/', search, name='search'),
    path('post/<int:pk>/', PostDetailView.as_view(), name="post-detail"),
    path('post/new/', PostCreateView.as_view(), name="post-create"),
    path('post/<int:pk>/update/', PostUpdateView.as_view(), name="post-update"),
    path('post/<int:pk>/delete/', PostDeleteView.as_view(), name="post-delete"),
    path('user/<str:username>/', UserPostListView.as_view(), name="user-posts"),
    path('about/', views.about, name='blog-about'),
    path('like', views.like_post, name="like_post"),
]

